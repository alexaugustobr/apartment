import org.springframework.cloud.contract.spec.Contract

Contract.make {

    input {
        label 'receive_event'
        triggeredBy 'fireEvent()'
    }

    outputMessage {
        sentTo 'apartments'

        body([
                buildingId: 35
        ])
    }

}