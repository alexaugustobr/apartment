package br.com.caelum.apartment.api.registration;

import br.com.caelum.apartment.domain.Apartment;
import br.com.caelum.apartment.shared.Mapper;
import org.springframework.stereotype.Component;

@Component
class ApartmentInputMapper implements Mapper<RegistrationController.ApartmentInput, Apartment> {
    @Override
    public Apartment map(RegistrationController.ApartmentInput input) {
        return new Apartment(input.getBuildingId(), input.getNumber(), input.getIdentifier());
    }
}
